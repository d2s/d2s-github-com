
2.0.0 / 2012-01-20 
==================

  * Move away from YUI Reset to Skeleton V1.1 Reset
  * Add forgotten box-shadow
  * Added source links for CSS3 patterns
  * New layout
  * Add prefixfree.min.js
  * Convert tabs to spaces
  * Just a test...
  * Small cleanup of extra spaces from index.html
  * Commented out @import of reset.css from screen.css
  * Created simple layout for the userpage.
  * First commit!
